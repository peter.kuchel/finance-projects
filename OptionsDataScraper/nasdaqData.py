import pandas as pd 
import os.path
from yahoo_fin import stock_info
import json 

filesNames = {

    'raw_file': "nasdaq_screener_1617169648587.csv",
    'save_file': "accepted_tickers.csv", 
    'pickle_file': "nasdaqPE",
    'json_file': "scrapesymbols.json"
}

class NasdaqTickerHandler(object):

    def __init__(self, csv_file, save_file, pickle_file):
        self.csv_file = csv_file
        self.save_file = save_file
        self.pickle_file = pickle_file
    
    def __collect_PE_data(self, ticker):
        print(f"---{ticker}---")
        data = stock_info.get_quote_table(ticker)
        return data
        #['PE Ratio (TTM)', 'EPS (TTM)']

    def __open_pickle(self, file_name):
        return pd.read_pickle(file_name)

    def __save_pickle(self, df):
        return df.to_pickle(self.pickle_file)

    def __retrieve_final(self):
        if os.path.isfile(self.pickle_file):
            df_final = self.__open_pickle(self.pickle_file)
        else:
            df_final = pd.DataFrame(columns=['Symbol', 'Name', 'Country', 'P/E', 'EPS'])
        
        return df_final

    def __get_currently_checked(self):
        df_final = self.__retrieve_final()
        return {t for t in df_final['Symbol']}

    def __set_values(self, subsect, yh_data):
        subsect_copy = subsect.copy()
        for (df_item, yh_item) in zip(['P/E', 'EPS'], ['PE Ratio (TTM)', 'EPS (TTM)']):
            try: 
                subsect_copy.loc[:,df_item] = yh_data[yh_item]
            except KeyError:
                subsect_copy.loc[:,df_item] = 'NaN'

        return subsect_copy

    def read_and_collect(self):
        
        df = pd.read_csv(self.csv_file)
        df_tickers = df.loc[:,['Symbol', 'Name', 'Country']]

        all_tickers = {t for t in df_tickers['Symbol']}
        currently_checked = self.__get_currently_checked()
        tickers_to_find = all_tickers.difference(currently_checked)

        for ticker in tickers_to_find:
            df_final = self.__retrieve_final()
            subsect = df_tickers.loc[lambda df: df['Symbol'] == ticker,:] 

            yh_data = self.__collect_PE_data(ticker)
            subsect_copy = self.__set_values(subsect, yh_data)

            # print(subsect_copy)
            df_final = pd.concat([df_final]+[subsect_copy], ignore_index = True)
            self.__save_pickle(df_final)

        df_final = self.__retrieve_final()
        df_final = df_final.dropna()
        # print(df_final)
        df_acceptable = df_final.loc[lambda df: df['EPS'] > 1, :]
        df_acceptable = df_acceptable.loc[lambda df: df['P/E'] > 60, :]
        df_acceptable.to_csv(self.save_file)
        # print(df_acceptable)
        # print(len(df_acceptable))
    
    def symbols_to_json(self, json_file):
        df_final = pd.read_csv(self.save_file)
        just_symbols = df_final['Symbol']
        result = just_symbols.to_json(orient='columns')
        parsed = json.loads(result)
        with open(json_file, 'w') as jf:
            json.dump(parsed, jf, indent=2)


if __name__ == '__main__':

    rawfile, savefile, picklefile, jsonfile = filesNames.values()
    data = NasdaqTickerHandler(rawfile, savefile, picklefile)
    # data.read_and_collect()
    data.symbols_to_json(jsonfile)
   

