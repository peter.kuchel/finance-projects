const { selectors, firefox } = require("playwright");
const fs = require("fs");


const INFILEPATH = "scrapesymbols.json";
const SAVEPATH = "optionsData.json"

const DEBUG = false;

const paths = {
    'dropdown': 'button.option-chain-filter-toggle-month.option-chain-tables__dropdown-toggle',
    'selectAll': '[aria-label="Click to show data for All"]',
    'tbody': 'tbody.option-chain-tables__table-body',
    'nextPage': '[aria-label="click to go to the next page"]'
}

const createURl = (ticker) => {
    return `https://www.nasdaq.com/market-activity/stocks/${ticker}/option-chain`
}

const readData = async (path) => {
    return fs.readFile(path, { encoding: 'utf8' }, (err, data) => {
        if (err) {
            console.log(err);
        } else {
            return JSON.parse(data);
        }
    });

}

const formatData = (contracts) => {
    return contracts.map((contract) => {
        const contractValues = contract.split('\t');
        const expiry = contractValues[0];
        const callData = contractValues;
    });
}

// get the current tbody layout s
const getTbodyLayout = async (currentPage) => {
    // let notEmpty = false;
    await currentPage.waitForSelector(paths['tbody']);
    return await currentPage.innerHTML(paths['tbody']);
}

// check the last entry on the tbody as a reference point 
const checkLastEntry = async (currentPage) => {
    const tbody = await currentPage.waitForSelector(paths['tbody']);
    let alltrTags = await tbody.$$('tr');
    return await alltrTags[alltrTags.length - 1].innerHTML();
}

// load the 'All' option 
const loadAll = async (currentPage, refTbody) => {
    await currentPage.click(paths['dropdown']),
        await currentPage.click(paths['selectAll']); //{noWaitAfter:false}

    await currentPage.waitForSelector(paths['tbody'], { visible: true });
    let isAll = false;

    // load the 'All' tbody 
    do {
        let currentDom = await getTbodyLayout(currentPage);
        if (currentDom !== refTbody) {
            isAll = true;
        }
    } while (!isAll);
}

const collectAll = async (currentPage, refEntry) => {
    allCollected = false;
    let allContracts = [];
    do {

        const allOnPage = await currentPage.$eval(paths['tbody'], (data) => {
            let contracts = [];
            const trTags = data.getElementsByTagName('tr');

            trTags.forEach((tag) => {
                contracts.push(tag.innerText);
            });

            return contracts;
        });

        if (DEBUG)
            console.log(allOnPage); // print the contracts on that page
        allContracts.push(allOnPage);
        if (DEBUG)
            console.log(allContracts.length); //

        let newData = false;
        await currentPage.click(paths['nextPage'], { timeout: 5000 })
            .catch(() => {
                console.log('last page');
                newData = true;
                allCollected = true;
            });
        while (!newData) {
            let currentTR = await checkLastEntry(currentPage);

            if (currentTR !== refEntry) {
                if (DEBUG)
                    console.log('no timeout');

                newData = true;
                refEntry = currentTR;
            }
        }

    } while (!allCollected);

    return allContracts;
}

(async () => {

    const bufferString = fs.readFileSync(INFILEPATH, { encoding: "utf-8" });
    const allTickers = JSON.parse(bufferString);

    console.log(allTickers);

    let ops = {
        headless: true // for debugging 
    };

    const browser = await firefox.launch(ops);
    const context = await browser.newContext();

    const tickers = Object.values(allTickers);
    console.log(tickers);

    let results = {}
    for (const ticker of tickers.values()) {

        console.log(ticker);
        const url = createURl(ticker);
        const currentPage = await context.newPage();

        await currentPage.goto(url, { waitUntil: 'domcontentloaded' }) //
            .catch((err) => {
                console.log(err);
            });

        // get current copy of tbody in the dom 
        let currentTbody = await getTbodyLayout(currentPage);

        // keep reference of the first row in the tbody 
        let currentLoad = await checkLastEntry(currentPage);
        if (DEBUG)
            console.log('Here is the last entry before all loaded \n' + currentLoad); //debugging 

        // make sure that 'ALL' is loaded 
        await loadAll(currentPage, currentTbody);

        currentLoad = await checkLastEntry(currentPage);
        if (DEBUG)
            console.log('Here is the first entry after all loaded \n' + currentLoad); //debugging 

        // scrape each of the pages
        let allContrcts = await collectAll(currentPage, currentLoad);
        results[ticker] = allContrcts;

        console.log(`Pages scraped: ${allContrcts.length}`);
        console.log(`finished for ${ticker}`);
        await currentPage.close();

    }
    console.log('closing browser');
    await browser.close();
    let saveResults = JSON.stringify(results, null, 4);
    fs.writeFileSync(SAVEPATH, saveResults, "utf-8");


})();



// OLD CODE FOR REFERENCE //

// allCollected = false;
// let allContracts = [];
// do {
//     const allOnPage = await currentPage.$eval(paths['tbody'], (data)=>{
//         let contracts = [];
//         const trTags = data.getElementsByTagName('tr');
//         trTags.forEach((tag)=>{
//             contracts.push(tag.innerText);
//         });
//         return contracts;
//     });
//     console.log(allOnPage);
//     allContracts.push(allOnPage)
//     console.log(allContracts.length);
//     let newData = false;
//     await currentPage.click(paths['nextPage'], {timeout:5000})
//                         .catch(()=>{
//                             console.log('last page');
//                             newData = true;
//                             allCollected = true;
//                         });
//     while (!newData){
//         let currentTR = await checkTableData(currentPage).catch((err)=>console.log(err));
//         if (currentTR !== currentLoad){
//             console.log('no timeout');
//             newData = true;
//             currentLoad = currentTR;
//         }
//     }
// } while (!allCollected);
// console.log(`finished for ${ticker}`);
// await currentPage.close();



// const checkTableData = async (currentPage)=>{
//     return currentPage.innerHTML(paths['tbody']);
// }



// const collectAllContracts = async (currentPage, bodyRef) =>{
//     let allCollected = false
//     let allContracts = [];
//     do {
//         await currentPage.click(paths['nextPage'], {timeout:5000})
//                          .catch(()=>{
//                             console.log('last page');
//                             allCollected = true;
//                           });
//     } while(!allCollected);
// }
