import pandas as pd
import numpy as np 
import math 
import datetime as dt 
from random import randrange
from yahoo_fin import stock_info
from scipy.stats import norm

"""
A simple Monte Carlo (MC) to model VaR and CVaR for portfolios. Can create a MC model for any portfolio of publicly traded companies on the NYSE, NASDAQ, TSX, and TSXV. Assumes normal distribution of stock returns.

Script has two classes:
MonteCarlo: to model for a single security
MonteCarloMulti: to model for a portfolio
"""

class consts(object):
    MODUS = 2**31 - 1 
    MULTIPLIER = 7**5 
    TRADING_DAYS_PER_YEAR = 252 
    VAR_CI = 0.95 # 95% confidence interval 

class dateUtil(object):

    def __init__(self):
        pass 
    
    def current_date(self):
        start = dt.datetime.today()
        return start.strftime("%Y-%m-%d")
    
    def convert_year_to_date(self, years):
        dt_ob = dt.datetime.today()
        five_years = 365*years
        time_delta = dt.timedelta(days=five_years)
        past_date = dt_ob - time_delta
        return past_date.strftime("%Y-%m-%d")

class MonteCarlo(object):

    def __init__(self, ticker, to_gen=10_000, period=None):
        self.ticker = ticker
        self.dateUtil = dateUtil()
        self.consts = consts()
        self.to_gen = to_gen 
        self.period = period
        self.single_mc_df = None 
        
    def __get_historic_data(self):
        if self.period !=None:
            start = self.dateUtil.convert_year_to_date(self.period)
            data = stock_info.get_data(ticker=self.ticker, start_date=start, index_as_date=False)
        else:
             data = stock_info.get_data(ticker=self.ticker, index_as_date=False) 

        date = data['date']
        adj = data['adjclose']
        adj.columns = [self.ticker]
        return pd.concat([date,adj], axis=1)
    
    def __generate_values(self):
        m = self.consts.MODUS 
        mul = self.consts.MULTIPLIER 

        seed = randrange(10, 50_000)
        prelim = np.arange(self.to_gen, dtype=np.int64)

        prelim_calc = lambda x: (mul * x) % m

        for p in prelim:
            if p == 0:
                prelim[p] = prelim_calc(seed)
            else:
                prelim[p] = prelim_calc(prelim[p-1])
        
        df = pd.DataFrame(prelim, columns=['prelim'])
        df['rand'] = df['prelim'].apply(lambda x: x / m)
        df['norminv'] = df['rand'].apply(lambda x: norm.ppf(x))

        return df 
    
    def __calc_historic_base(self, historic_df):

        historic_df['return'] = historic_df['adjclose'].pct_change(1)
        daily_mean = historic_df['return'].mean()
        daily_std = historic_df['return'].std()

        trading_days = self.consts.TRADING_DAYS_PER_YEAR 

        time_increment = 1 / trading_days 
        annualized_mean = trading_days * daily_mean 
        annualized_std = daily_std * math.sqrt(trading_days)
        er = annualized_mean - ( (annualized_std)**2 / 2)  #expected return

        brownian_motion_func = lambda n: (er * time_increment) + (annualized_std * n * math.sqrt(time_increment))
        return brownian_motion_func
    
    @staticmethod
    def cvar(just_returns, to_gen):
        var_CI = consts.VAR_CI
        cvar_range = int(to_gen *(1-var_CI)) - 1
        cvar_vals = just_returns[:cvar_range].sum()
        return cvar_vals * (1 / cvar_range)
    
    @staticmethod
    def var(just_returns, to_gen):
        var_CI = consts.VAR_CI
        value = int(to_gen * (1-var_CI))
        var_val = just_returns.sort_values(ascending=True).to_numpy()
        return var_val[value] 

    @staticmethod 
    def return_results(just_returns, to_gen):
        td = consts.TRADING_DAYS_PER_YEAR

        df_parts = {
            'Highest': just_returns.max(),
            'Lowest': just_returns.min(),
            'Avg': just_returns.mean() * math.sqrt(td),
            'Std': just_returns.std() * math.sqrt(td),
            'VaR': MonteCarlo.var(just_returns, to_gen),
            'CVaR': MonteCarlo.cvar(just_returns, to_gen)
        }
        
        return pd.DataFrame(df_parts, index=['results'])
    
    def mc_returns(self):
        return self.single_mc_df['return']
    
    def return_full_df(self):
        return self.single_mc_df
    
    def return_full(self):
        mc_returns = self.mc_returns()
        results = MonteCarlo.return_results(mc_returns, self.to_gen)
        return (self.mc_returns(), results)

    def calc_monte_carlo(self):

        df = self.__get_historic_data()
        bm_func = self.__calc_historic_base(df)
        mc_df = self.__generate_values()

        mc_df['return'] = mc_df['norminv'].apply(lambda x: bm_func(x))
        self.single_mc_df = mc_df 


class MonteCarloMulti(object):

    def __init__(self, tickers, weights, period=5, to_gen=10_000):
        """
        @params:
            -tickers: list of tickers 
            -weights: list of weights assigned to each ticker
                      (size of tickers and weights should be the same, and should be in order)
            -period: how far back to look for the returns of each ticker
            -to_gen: how many returns for the MC to generate 
        """
        if len(tickers) != len(weights):
            raise Exception("Weights must be the same length as tickers")
        if sum(weights) != 1:
            raise Exception("Weights must be equal to one")

        self.tickers = tickers
        self.weights = weights 
        self.to_gen = to_gen
        self.period = period
        self.df = None 
    
    def __calc_weighted_avg(self, row):
        pairs = zip(self.tickers, self.weights)
        weighted_returns = [row[t]*w for (t,w) in pairs]
        return sum(weighted_returns)

    def calc_monte_carlo(self):
        obs = []
        for t in self.tickers: 
            mc = MonteCarlo(t, self.to_gen, self.period)
            mc.calc_monte_carlo()
            result = mc.mc_returns()
            obs.append(result)
        
        self.df = pd.concat(obs, axis=1) 
        self.df.columns = self.tickers 
        self.df['AVG'] = self.df.apply(lambda x: self.__calc_weighted_avg(x), axis=1)
        results = self.return_results()
        print(results)
    
    def return_results(self):
        if self.df == None:
            raise Exception("The Dataframe for the model is empty")
        return MonteCarlo.return_results(self.df['AVG'], self.to_gen)

    def return_full_df(self):
        return self.df

if __name__ == '__main__':

    ### example provided ### 
    portfolio = ['AAPL', 'NUMI.V', 'TD.TO', 'GE']
    weights = [0.2, 0.5, 0.2, 0.1]

    mc = MonteCarloMulti(portfolio, weights, to_gen=10_000)
    mc.calc_monte_carlo()

    

